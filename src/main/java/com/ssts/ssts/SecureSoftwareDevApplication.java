package com.ssts.ssts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecureSoftwareDevApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecureSoftwareDevApplication.class, args);
	}

}
