package com.ssts.ssts.config;

import com.ssts.ssts.model.Comment;
import com.ssts.ssts.model.Role;
import com.ssts.ssts.model.Ticket;
import com.ssts.ssts.model.Users;
import com.ssts.ssts.repository.CommentRepository;
import com.ssts.ssts.repository.RoleRepository;
import com.ssts.ssts.repository.TicketRepository;
import com.ssts.ssts.repository.UsersRepository;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;


@Component
public class DataGenerator implements CommandLineRunner{


        @Autowired
        TicketRepository ticketRepository;

        @Autowired
        UsersRepository usersRepository;

        @Autowired
        CommentRepository commentRepository;

        @Autowired
        RoleRepository roleRepository;

        public DataGenerator() {
        }

        @Override
        public void run(String... args) throws Exception {

            ticketRepository.deleteAll();
            usersRepository.deleteAll();
            commentRepository.deleteAll();
            roleRepository.deleteAll();

            createRoles();
            createTickets();
            createUsers();
            createComments();
        }

        private void createRoles() {
            Role role = new Role("1", "ADMIN");
            Role role1 = new Role("2", "USER");
            Role role2 = new Role("3", "OTHER_USER");

            roleRepository.save(role);
            roleRepository.save(role1);
            roleRepository.save(role2);
        }

        private void createTickets() {

            ArrayList<String> commentIds = new ArrayList<>();
            commentIds.add("D-123-1");
            commentIds.add("D-123-2");

            ArrayList<String> commentIds1 = new ArrayList<>();
            commentIds.add("D-124-1");

            Ticket ticket = new Ticket("D-123", "This is a generated description value", "Tester203", "Developer234", "Open", "Low", "Development", commentIds, "Nov 05 2019 17:34" );
            Ticket ticket1 = new Ticket("D-124", "This is a generated description value", "Tester204", "Developer234", "Closed", "High", "Development", commentIds1, "Tue Nov 05 2019 17:48");
            Ticket ticket2 = new Ticket("D-125", "This is a generated description value", "Tester205", "Developer234", "Open", "Low", "Development", "Tue Nov 05 2019 17:52");

            ticketRepository.save(ticket);
            ticketRepository.save(ticket1);
            ticketRepository.save(ticket2);
        }

        private void createUsers() {

            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            String adminPassword = "sstcwadmin2850";
            String testerPassword = "sstcwtester2987";
            String developerPassword = "sstcwdeveloper9643";
            String clientPassword = "sstcwclient0754";

            String adminHashedPassword = passwordEncoder.encode(adminPassword);
            String testerHashedPassword = passwordEncoder.encode(testerPassword);
            String developerHashedPassword = passwordEncoder.encode(developerPassword);
            String clientHashedPassword = passwordEncoder.encode(clientPassword);

            Users admin = new Users("Admin",  adminHashedPassword, "1");
            Users developer = new Users("Developer302",  developerHashedPassword, "2");
            Users tester = new Users("Tester409",  testerHashedPassword, "2");
            Users client = new Users("Client234",  clientHashedPassword, "3");

            usersRepository.save(admin);
            usersRepository.save(developer);
            usersRepository.save(tester);
            usersRepository.save(client);

        }

        private void createComments() {

            Comment comment = new Comment("tester", "Comment-Desc", "D-123", "D-123-1", "Tue Nov 05 2019");
            Comment comment1 = new Comment("tester", "Comment-Desc Second Comment", "D-123", "D-123-2", "Tue Nov 05 2019");
            Comment comment2 = new Comment("admin", "Comment-Desc", "D-124", "D-124-1", "Tue Nov 05 2019");


            commentRepository.save(comment);
            commentRepository.save(comment1);
            commentRepository.save(comment2);
        }


    }

