package com.ssts.ssts.controller;

import com.google.gson.Gson;
import com.ssts.ssts.model.Comment;
import com.ssts.ssts.model.Ticket;
import com.ssts.ssts.service.CommentService;
import com.ssts.ssts.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://localhost:4200")
@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private TicketService ticketService;

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public void addComment(@RequestBody String comment){

        Comment gson = new Gson().fromJson(comment, Comment.class);
        ticketService.addCommentId(gson.getCommentId(), gson.getTicketNumber());
        commentService.addComment(gson);

    }



    @RequestMapping(value = "/getComments/{ticketNumber}", method = RequestMethod.GET)
    public List<Comment> getComments(@PathVariable String ticketNumber) {
        Ticket currentTicket = ticketService.getTicketByName(ticketNumber);
        return getCommentsFromTicket(currentTicket);


    }


    private List<Comment> getCommentsFromTicket(Ticket currentTicket) {
        if (currentTicket.getCommentIds() != null) {
            ArrayList<String> commentIds = currentTicket.getCommentIds();

            if (commentIds.size() > 0) {
                return commentService.getComments(commentIds);
            }
        }

        return new ArrayList<>();

    }
}
