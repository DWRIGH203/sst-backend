package com.ssts.ssts.controller;

import com.google.gson.Gson;
import com.ssts.ssts.model.Comment;
import com.ssts.ssts.model.Role;
import com.ssts.ssts.model.Ticket;
import com.ssts.ssts.service.CommentService;
import com.ssts.ssts.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins = "https://localhost:4200")
@RestController
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private JwtAuthenticationController jwtAuthenticationController;

    private String user_role;
    private static final String OTHER_USER = "OTHER_USER";


    @RequestMapping(value = "/addTicket", method = RequestMethod.POST)
    public void createNewTicket(@RequestBody String ticket){

    if(!user_role.equals(OTHER_USER)) {
        Ticket gson = new Gson().fromJson(ticket, Ticket.class);
        ticketService.addTicket(gson);
    }

    }

    @RequestMapping(value = "/getAllTickets", method = RequestMethod.GET)
    public List<Ticket> getAllTickets() {
        return ticketService.getAllTickets();
    }

    @RequestMapping(value = "/deleteTicket", method = RequestMethod.POST)
    public void deleteTicket(@RequestBody String ticketNumber) {

        if(!user_role.equals(OTHER_USER)) {
            Ticket gson = new Gson().fromJson(ticketNumber, Ticket.class);
            ticketService.deleteTicket(gson.getTicketNumber());
        }
    }

    @RequestMapping(value = "/getTicketByName/{ticketNumber}", method = RequestMethod.GET)
    public Ticket getTicketByName(@PathVariable String ticketNumber) {
        Ticket requiredTicket = ticketService.getTicketByName(ticketNumber);

        if (requiredTicket.getCommentIds() != null) {
            ArrayList<String> commentIds = requiredTicket.getCommentIds();
            getCommentsForTicket(commentIds);
        }

        return requiredTicket;

    }

    @RequestMapping(value = "/editTicket", method = RequestMethod.PUT)
    public void editTicket(@RequestBody String ticket) {
        if(!user_role.equals(OTHER_USER)) {
            Ticket gson = new Gson().fromJson(ticket, Ticket.class);
            ticketService.editTicket(gson);
        }
    }

    private List<Comment> getCommentsForTicket(List<String> commentIds) {
        if(commentIds.size() > 0) {
            return commentService.getComments(commentIds);
        }
        return new ArrayList<>();
    }

    public void roleSet(String username) {
       Role role =  jwtAuthenticationController.getRole(username);
       user_role =  role.getRole();
    }

}

