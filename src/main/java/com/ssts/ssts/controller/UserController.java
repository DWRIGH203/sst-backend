package com.ssts.ssts.controller;

import com.ssts.ssts.model.User;
import com.ssts.ssts.model.Users;
import com.ssts.ssts.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://localhost:4200")
@RestController
public class UserController {


    @Autowired
    private UserService userService;


    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
    public List<User> getAllUsers(){

        List<Users> allUsers = userService.getAllUsers();
        List<User> users = new ArrayList<>();

        for(Users u: allUsers) {
            User user = new User();
            user.setUsername(u.getUsername());
            user.setRole(u.getRoleId());

            users.add(user);
        }

        return users;

    }

}
