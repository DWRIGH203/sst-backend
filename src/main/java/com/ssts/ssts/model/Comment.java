package com.ssts.ssts.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ssd_comments")
public class Comment {

    @Id
    private ObjectId _id;

    private String name;
    private String commentValue;

    private String ticketNumber;

    private String commentId;

    private String timestamp;

    public Comment(ObjectId _id, String name, String commentValue, String commentId, String ticketNumber, String timestamp) {
        this._id = _id;
        this.name = name;
        this.commentValue = commentValue;
        this.commentId = commentId;
        this.ticketNumber = ticketNumber;
        this.timestamp = timestamp;
    }

    public Comment(String name, String commentValue, String ticketNumber, String commentId, String timestamp) {
        this.name = name;
        this.commentValue = commentValue;
        this.commentId = commentId;
        this.ticketNumber = ticketNumber;
        this.timestamp = timestamp;
    }

    public Comment() {};

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommentValue() {
        return commentValue;
    }

    public void setCommentValue(String commentValue) {
        this.commentValue = commentValue;
    }

}
