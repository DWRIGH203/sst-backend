package com.ssts.ssts.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ssd_roles")
public class Role {

    @Id
    private ObjectId _id;

    private String roleId;

    private String role;

    public Role(ObjectId _id, String roleId, String role) {
        this.roleId = roleId;
        this.role = role;
        this._id = _id;
    }

    public Role(String roleId, String role) {
        this.roleId = roleId;
        this.role = role;
    }

    public Role() {}

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

}
