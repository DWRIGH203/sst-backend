package com.ssts.ssts.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;


@Document(collection = "ssd_tickets")
public class Ticket {

    @Id
    private ObjectId _id;

    private String ticketNumber;
    private String timestamp;
    private String description;
    private String bugFinder;
    private String assignee;
    private String status;
    private String priority;
    private String type;
    private ArrayList<String> commentIds;



    public Ticket(ObjectId _id, String ticketNumber, String timestamp, String description, String bugFinder, String assignee, String status, String priority, String type, ArrayList<String> commentIds) {
        this._id = _id;
        this.ticketNumber = ticketNumber;
        this.timestamp = timestamp;
        this.description = description;
        this.bugFinder = bugFinder;
        this.assignee = assignee;
        this.status = status;
        this.priority = priority;
        this.type = type;
        this.commentIds = commentIds;
    }

    public Ticket(String ticketNumber, String description, String bugFinder, String assignee, String status, String priority, String type, ArrayList<String> commentIds, String timestamp) {
        this.ticketNumber = ticketNumber;
        this.description = description;
        this.bugFinder = bugFinder;
        this.assignee = assignee;
        this.status = status;
        this.priority = priority;
        this.type = type;
        this.commentIds = commentIds;
        this.timestamp = timestamp;

    }

    public Ticket(String ticketNumber, String description, String bugFinder, String assignee, String status, String priority, String type, String timestamp) {
        this.ticketNumber = ticketNumber;
        this.description = description;
        this.bugFinder = bugFinder;
        this.assignee = assignee;
        this.status = status;
        this.priority = priority;
        this.type = type;
        this.timestamp = timestamp;

    }

    public Ticket(){}


    public void set_id(ObjectId _id) { this._id = _id; }

    public String get_id() { return this._id.toHexString(); }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBugFinder() {
        return bugFinder;
    }

    public void setBugFinder(String bugFinder) {
        this.bugFinder = bugFinder;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public ArrayList<String> getCommentIds() {
        return commentIds;
    }

    public void setCommentId(ArrayList<String> commentId) {
        this.commentIds = commentId;
    }









}
