package com.ssts.ssts.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ssd_users")
public class Users {
    @Id
    public ObjectId _id;

    public String username;
    public String password;

    public String roleId;

    public Users() {}

    public Users(ObjectId _id, String username, String password, String roleId) {
        this._id = _id;
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }
    public Users(String username, String password, String roleId) {
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String role) {
        this.roleId = roleId;
    }

    public void set_id(ObjectId _id) { this._id = _id; }

    public String get_id() { return this._id.toHexString(); }

    public void setPassword(String password) { this.password = password; }

    public String getPassword() { return password; }

    public void setUsername(String username) { this.username = username; }

    public String getUsername() { return username; }
}
