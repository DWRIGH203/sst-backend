package com.ssts.ssts.repository;

import com.ssts.ssts.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {
}
