package com.ssts.ssts.repository;

import com.ssts.ssts.model.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsersRepository extends MongoRepository<Users, String> {

    Users findByUsername(String username);
}
