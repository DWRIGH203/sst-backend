package com.ssts.ssts.service;

import com.ssts.ssts.model.Comment;

import java.util.List;

public interface CommentService {

    void addComment(Comment comment);
    List<Comment> getComments(List<String> commentIds);
}
