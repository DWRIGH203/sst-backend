package com.ssts.ssts.service;

import com.ssts.ssts.model.Role;
import com.ssts.ssts.model.Users;

public interface RoleService {

    Role getUserRole(String username);
}
