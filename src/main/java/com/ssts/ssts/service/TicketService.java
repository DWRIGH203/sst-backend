package com.ssts.ssts.service;

import com.ssts.ssts.model.Ticket;

import java.util.List;

public interface TicketService {

    List<Ticket> getAllTickets();
    void addTicket(Ticket ticket);
    void deleteTicket(String ticketNumber);
    //Add validation so that they are no duplicates
    Ticket getTicketByName(String ticketNumber);
    void addCommentId(String commentId, String ticketNumber);
    void editTicket(Ticket ticket);
}
