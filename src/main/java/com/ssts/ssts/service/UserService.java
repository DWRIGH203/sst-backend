package com.ssts.ssts.service;

import com.ssts.ssts.model.Users;
import java.util.List;

public interface UserService {

    List<Users> getAllUsers();
}
