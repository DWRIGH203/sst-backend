package com.ssts.ssts.service.impl;

import com.ssts.ssts.model.Comment;
import com.ssts.ssts.model.Ticket;
import com.ssts.ssts.repository.CommentRepository;
import com.ssts.ssts.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public void addComment(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public List<Comment> getComments(List<String> commentIds) {
        List<Comment> requiredComments = new ArrayList<>();
        List<Comment> allComments = commentRepository.findAll();

    if (allComments.size() > 0) {
        for (Comment c : allComments) {
            if (commentIds.contains(c.getCommentId())) {
                requiredComments.add(c);
            }
        }
    }

        return requiredComments;

    }

}
