package com.ssts.ssts.service.impl;

import com.ssts.ssts.model.Role;
import com.ssts.ssts.model.Users;
import com.ssts.ssts.repository.RoleRepository;
import com.ssts.ssts.repository.UsersRepository;
import com.ssts.ssts.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {


    @Autowired
    UsersRepository usersRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Role getUserRole(String username) {
        Users user = usersRepository.findByUsername(username);
        String roleId = user.getRoleId();

        List<Role> roleList = roleRepository.findAll();

        for(Role r: roleList) {
            if (r.getRoleId().equals(roleId)) {
                return r;
            }
        }

        return null;

    }
}
