package com.ssts.ssts.service.impl;

import com.ssts.ssts.model.Ticket;
import com.ssts.ssts.repository.TicketRepository;
import com.ssts.ssts.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Override
    public List<Ticket> getAllTickets() {
        List<Ticket> listByPriority = ticketRepository.findAll();

        return listByPriority;
    }

    @Override
    public void addTicket(Ticket ticket) {
        ticketRepository.save(ticket);
    }

    @Override
    public void deleteTicket(String ticketNumber) {

        List<Ticket> allTickets = getAllTickets();

        for (Ticket t : allTickets) {
            if(t.getTicketNumber().equals(ticketNumber)) {
                ticketRepository.delete(t);
            }
        }
    }

    @Override
    public Ticket getTicketByName(String ticketNumber) {
        List<Ticket> allTickets = ticketRepository.findAll();

        for(Ticket t : allTickets) {
            if(t.getTicketNumber().equals(ticketNumber)) {
                return t;
            }
        }

        return null;
    }

    @Override
    public void addCommentId(String commentId, String ticketNumber) {
        Ticket ticket = getTicketByName(ticketNumber);

        if(ticket.getCommentIds() != null) {
            ArrayList<String> commentIdList = ticket.getCommentIds();
            commentIdList.add(commentId);
            ticket.setCommentId(commentIdList);
        } else {
            ArrayList<String> commentIdList = new ArrayList<>();
            commentIdList.add(commentId);
            ticket.setCommentId(commentIdList);
        }


        ticketRepository.save(ticket);
    }

    @Override
    public void editTicket(Ticket ticket) {
        Ticket ticketUpdated = getTicketByName(ticket.getTicketNumber());

        if (ticket.getAssignee() != null) {
            ticketUpdated.setAssignee(ticket.getAssignee());
        }

        if(ticket.getPriority() != null) {
            ticketUpdated.setPriority(ticket.getPriority());
        }

        if(ticket.getStatus() != null) {
            ticketUpdated.setStatus(ticket.getStatus());
        }

        ticketRepository.save(ticketUpdated);

    }
}
