package com.ssts.ssts.service.impl;

import com.ssts.ssts.model.Users;
import com.ssts.ssts.repository.UsersRepository;
import com.ssts.ssts.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UsersRepository usersRepository;

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();

    }
}
